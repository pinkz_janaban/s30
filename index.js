//Step 7 import express
const express = require('express')

const mongoose = require('mongoose');

//Step 8 create express app
const app = express();

//Step 9, create a const called PORT
const port = 3001;

//Step 10 setup server to handle data from requests
//Allows our app to read JSON data
app.use(express.json())


app.use(express.urlencoded({extended:true}));

app.listen(port, ()=> console.log(`Server running at port ${port}`));


//Step 12 install Mongoose npm install mongoose by typing "npm install mongoose" in git bash
//Mongoose is a package that allows creation of Schemas to model our data structures

//Step 13 import mongoose
//const mongose = require('mongoose')

//Step 14 Go to MongoDB Atlas and change the Network access to 0.0.0.0

//Step 15 Get connection string and change the password to your password
//mongodb+srv://admin:admin1234@zuitt-bootcamp.fwkne.mongodb.net/myFirstDatabase?retryWrites=true&w=majority


//Step 16 change myFirstDatabase to s30. MongoDB will automatically create the database for us
//mongodb+srv://admin:admin1234@zuitt-bootcamp.fwkne.mongodb.net/s30?retryWrites=true&w=majority


//Step 17 Connecting to MongoDB Atlas -add .connect() method
//mongoose.connect();

//Step 18a Add the connection string as the 1st arguement
//step 18b Add this object to allow connection
/*
	{
	useNewUrlParser:true,
	useUnifiedTopology:true
	}
*/
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.fwkne.mongodb.net/s30?retryWrites=true&w=majority',
	{
	useNewUrlParser:true,
	useUnifiedTopology:true
	}
);

//Step 19 Set notification for connection success or failure by using .connection property of mo
//mongoose.connection;

//Step 20 store it in a variable called db.
let db = mongoose.connection;

//Step 21 console.error.bind(console) allows us to print errors in the browser console and in the terminal
db.on("error", console.error.bind(console, "connection error"));

//Step 22 if the connection is successful, output this in the console
db.once("open", ()=> console.log("We're connected to the cloud database"))

//Step 23 Schemas determine the structures of the documents to be written in the database
//Schemas act as blueprints to our data
//Use the Schema() constructor of the Mongoose module to create a new Schema object
/*const taskSchema = new mongoose.Schema({
	//Define the fields with the corresponding data type
	//For a task, it needs a "task name" and "task status"
	//There is a field called "name" and its data type is "String"
	name: String, //string ang datatype nya
	//There is a field called "status" that is a "String" and the default value is "pending"
	status:{
		type: String,
		default: "pending"
	}
});
*/
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});



//Models use Schemas and they act as the middleman from the server(JS code) to our database
//Step 24 Create a model
//Models must be in singular form and capitalized
//The first parameter of the Mongoose model method indicates the collection
//in where to store the data

//The second parameter is used to specify the Schema/blueprint
//of the documents that will be stored in the MongoDB collection

//Using Mongoose, the package was programmed well enough that it
//automatically converts the singular form of the model name
//into a plural form when creating a collection in postman 

// the name of collection is the plural name of model, example, model name: Task collection name: Tasks
// model is just a representation of collection
// syntax: mongoose.model("name of model", name of schema)
//const Task = mongoose.model("Task", taskSchema)
const User = mongoose.model("User", userSchema);


/*
Business Logic
1. Add a functionality to check if there are duplicate tasks
	-if the task already exists in the database, we return an error
	-if the task doesn't exist in the database, we add it in the database
*/

//Step 25 Create route to add task
//app.post('/tasks', (req, res)=> {
//	req.body.name;
//});

/*
Example: Eat
Look into the tasks collection
Task.findOne()
Task.fineOne({name: 'Eat'})

HTML Form - POSTMan
req.body
req.body.name

Task.findOne({name: req.body.name}, (err, result)=>{
	

})
*/

//Step 26 Check if the task already exists
//Use the Task Model to interact with the tasks collections
app.post ('/signup', (req, res) => {
	User.findOne({name: req.body.name},{password: req.body.password}, (err, result) => {
		if (result != null && result.name == req.body.name){
			res.send ('Duplicate task found!')
		} else {
			let newUser = new User ({
				name: req.body.name
			})

			newUser.save((saveErr, savedTask) => {
				if (saveErr){
					return console.error(saveErr)
				} else {
					return res.status (201).send('New User created.')
				}

			})
		}

	})
})
/*
//Step 27 Get all tasks
//Get all the contents of the task collection via the Task model
//"find" is a Mongoose method that is similar to Mongodb
//"find", and an empty "{}" means it returns all the documents and stores
//them in the "result" parameter of the callback function


app.get('/tasks', (req,res)=>{resuresu
	Task.find({},(err,resu){
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({data:result})
		}
	})
})
*/